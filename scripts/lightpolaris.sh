#!/bin/sh

if [ "$(command -v curl)" = "" ]; then
  echo "'cov-download-coverity.sh' requires Client URL ('curl'). Please install it."
  exit 1;
fi

if [ "$(command -v jq)" = "" ]; then
  echo "'cov-download-coverity.sh' requires the command-line JSON processor ('jq'). Please install it."
  exit 1;
fi

if [ "$#" -ne 4 ]; then
    echo "Usage: cov-download-analysis.sh <POLARIS_INSTANCE> <POLARIS_TOKEN> <COVERITY_ANALYSIS_PLATFORM> <COVERITY_ANALYSIS_VERSION>"
    echo
    echo "Examples: cov-download-analysis.sh sipse abcdefghijklmnopqrstu0123456789fghijklmnopqrstuvwxyz win64   2019.09"
    echo "          cov-download-analysis.sh sipse abcdefghijklmnopqrstu0123456789fghijklmnopqrstuvwxyz linux64 2019.09"
    echo "          cov-download-analysis.sh sipse abcdefghijklmnopqrstu0123456789fghijklmnopqrstuvwxyz macosx  2019.06-5"
    exit 1
fi

JWT=$(curl -s -X POST -d "accesstoken=$2" "https://$1.polaris.synopsys.com/api/auth/authenticate" | jq -r '.jwt')
if [ "$JWT" = "" ]; then
  echo Unable to connect to Polaris. Please check the Polaris instance.
  exit 1
fi
if [ "$JWT" = "null" ]; then
  echo Unable to authenticate to Polaris. Please check the Polaris token.
  exit 1
fi

DOWNLOAD=$(curl -s -H "Authorization: Bearer $JWT" "https://$1.polaris.synopsys.com/api/tools/tools/cov-analysis/$3/$4")
if [ "$DOWNLOAD" = "null" ]; then
  echo Unable to retrieve download. Please check Coverity Analysis platform and version.
  exit 1
fi

URL=$(echo $DOWNLOAD | jq -r '.url')
FILENAME=$(echo $DOWNLOAD | jq -r '.fileName')
echo Downloading $FILENAME
curl "$URL" -o $FILENAME
