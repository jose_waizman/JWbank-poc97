@echo off
setlocal enabledelayedexpansion

call "C:\Synopsys\Google Drive\coverity\commands\setenv\python-3.7.9.bat"

REM Ruta de Java, Maven
SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_251
SET MAVEN_HOME=C:\Synopsys\Program Portables\apache-maven-3.6.0
SET PATH=%JAVA_HOME%\bin;%MAVEN_HOME%\bin;%PATH%

SET POLARIS_SERVER_URL=https://poc97.polaris.synopsys.com
SET POLARIS_ACCESS_EMAIL=EMAIL
SET POLARIS_ACCESS_PASSWORD=PASSWORD
SET POLARIS_CLI_VERSION=win64
SET POLARIS_CLI_DIR=C:\Synopsys\polaris_cli\bin
SET POLARIS_CLI_REFRESH=true

REM python pipeline.py --help
python pipeline.py --action analyze --quality-gates dev --project-dir C:\Synopsys\codes\my-samples\Java-eclipse\empty.maven

echo.   
echo Exit code: %ERRORLEVEL%
if "%ERRORLEVEL%" == "0" GOTO EOF
echo Build break!!!

:EOF