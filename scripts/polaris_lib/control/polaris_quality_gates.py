#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from abc import ABC, abstractmethod
import datetime
import json
import os
import sys
import time

from polaris_lib import polaris_common
from polaris_lib import polaris_util
from polaris_lib.control.polaris_rules import AbstractRule
from polaris_lib.control.polaris_rules import CweSansHigh
from polaris_lib.control.polaris_rules import OwaspHigh
from polaris_lib.control.polaris_rules import QualityHigh
from polaris_lib.control.polaris_rules import SecurityHigh
from polaris_lib.control.polaris_rules import VeryOldHigh

# -----------------------------------------------------------------------------

RULE_CWE_SANS_HIGH = 'cwe-sans-high'
RULE_OWASP_HIGH    = 'owasp-high'
RULE_QUALITY_HIGH  = 'quality-high'
RULE_SECURITY_HIGH = 'security-high'
RULE_VERY_OLD_HIGH = 'very-old-high'

RULES = [
    RULE_CWE_SANS_HIGH,
    RULE_OWASP_HIGH,
    RULE_QUALITY_HIGH,
    RULE_SECURITY_HIGH,
    RULE_VERY_OLD_HIGH
]

# -----------------------------------------------------------------------------


class AbstractQualityGatesAnalyzer(ABC):

    @abstractmethod
    def get_name(self) -> str:
        '''Get quality gates name'''
        pass

    @abstractmethod
    def analyze(self, issues: list) -> int:
        '''Analyze each issue in list, show messages and return exit code'''
        pass

    # -----------------------------------------------------------------------------

    def get_age(self, now, first_detected) -> int:
        date_time_obj = datetime.datetime.strptime(
            first_detected, '%Y-%m-%dT%H:%M:%S')

        first_detected_utc = date_time_obj.replace(
            tzinfo=datetime.timezone.utc)

        hours = (now - first_detected_utc.timestamp()) / 3600

        return hours

# -----------------------------------------------------------------------------


class AbstractQualityGatesAnalyzerFactory(ABC):

    @abstractmethod
    def valid_ids(self) -> list:
        '''Return a string list with valid ids for create(id)'''
        pass

    @abstractmethod
    def build(self, id: str) -> AbstractQualityGatesAnalyzer:
        '''Return a new AbstractQualityGatesAnalyzer or None if id is invalid'''
        pass

# -----------------------------------------------------------------------------


class QualityGatesAnalyzerFactory(AbstractQualityGatesAnalyzerFactory):

    def __init__(self):
        self.__quality_gates_configs = []
        self.__quality_gates_ids = []
        self.__load()

    # -------------------------------------------------------------------------

    def valid_ids(self) -> list:
        return self.__quality_gates_ids

    # -------------------------------------------------------------------------

    def build(self, id: str) -> AbstractQualityGatesAnalyzer:
        id = id.lower()
        for quality_gate_config in self.__quality_gates_configs:
            if (id == quality_gate_config.get('id')):
                return self.__build(quality_gate_config)

        return None

    # -------------------------------------------------------------------------

    def __build(self, quality_gate_config) -> AbstractQualityGatesAnalyzer:
        id = quality_gate_config.get('id')

        name = quality_gate_config.get('name')
        if (name is None):
            print(f"[Error] Quality gate '{id}' has no name")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        rules_config = quality_gate_config.get('rules')
        if (rules_config is None):
            print(f"[Error] Quality gate '{id}' has no rules")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        result = CustomQualityGatesAnalyzer(name)
        now = time.time()

        i = 0
        for rule_config in rules_config:
            rules_config_id = rule_config.get('id')
            if (rules_config_id is None):
                print(f"[Error] Rule {i} of quality gate '{id}' has no id")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            if (RULE_CWE_SANS_HIGH == rules_config_id):
                rule = self.__build_cwe_sans_high(rule_config)
            elif (RULE_OWASP_HIGH == rules_config_id):
                rule = self.__build_owasp_high(rule_config)
            elif (RULE_QUALITY_HIGH == rules_config_id):
                rule = self.__build_quality_high(rule_config)
            elif (RULE_SECURITY_HIGH == rules_config_id):
                rule = self.__build_security_high(rule_config)
            elif (RULE_VERY_OLD_HIGH == rules_config_id):
                rule = self.__build_very_old_high(rule_config, now)
            else:
                print(f"[Error] Unknow rule[{i}] id '{id}' of quality gate '{id}'")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            if (rule is None):
                print(f"[Error] Rule[{i}] of quality gate '{id}' is invalid")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            result.append_rule(rule)
            i += 1

        return result

    # -------------------------------------------------------------------------

    def __build_cwe_sans_high(self, rule_config: dict) -> CweSansHigh:
        max = int(rule_config.get('max'))
        return CweSansHigh(max)

    # -------------------------------------------------------------------------

    def __build_owasp_high(self, rule_config: dict) -> OwaspHigh:
        max = int(rule_config.get('max'))
        return OwaspHigh(max)

    # -------------------------------------------------------------------------

    def __build_quality_high(self, rule_config: dict) -> QualityHigh:
        max = int(rule_config.get('max'))
        return QualityHigh(max)

    # -------------------------------------------------------------------------

    def __build_security_high(self, rule_config: dict) -> SecurityHigh:
        max = int(rule_config.get('max'))
        rule = SecurityHigh(max)
        return rule

    # -------------------------------------------------------------------------

    def __build_very_old_high(self, rule_config: dict, now: float) -> VeryOldHigh:
        max = int(rule_config.get('max'))
        max_age_in_hours = int(rule_config.get('max_age_in_hours'))
        return VeryOldHigh(max, now, max_age_in_hours)

    # -------------------------------------------------------------------------

    def __load(self):
        filepath = polaris_util.build_config_filepath('quality-gates.json')
        if (not os.path.exists(filepath)):
            print(f"[Error] File not found '{filepath}'")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        data_json = polaris_util.read_file(filepath)

        i = 0
        try:
            self.__quality_gates_configs = json.loads(data_json)
        except json.decoder.JSONDecodeError as e:
            print(f"[Error] JSON file error '{filepath}': {e.msg}")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        if (self.__quality_gates_configs == []):
            print(f"[Error] There are no rules in the file '{filepath}'")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        for quality_gates_config in self.__quality_gates_configs:
            id = quality_gates_config.get('id')
            if (id is None):
                print(f"[Error] Quality gate [{i}] has no id")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            name = quality_gates_config.get('name')
            if (name is None):
                print(f"[Error] Quality gate [{i}] has no name")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            rules = quality_gates_config.get('rules')
            if ((rules is None) or (not isinstance(rules, list)) or (rules == [])):
                print(f"[Error] Quality gate [{i}] has no rules")
                sys.exit(polaris_common.EXIT_CODE_ERROR)

            for rule in rules:
                if (rule not in RULES):
                    valids = ' | '.join(rules)
                    print(f"[Error] Unknow rule id '{rule}' of quality gate '{id}'. Use: {valids}")
                    sys.exit(polaris_common.EXIT_CODE_ERROR)

            self.__quality_gates_ids.append(id)
            i += 1

# -----------------------------------------------------------------------------


class AbstractCustomQualityGatesAnalyzer(AbstractQualityGatesAnalyzer):
    def __init__(self):
        self._rules = []

    # -------------------------------------------------------------------------

    @abstractmethod
    def get_name(self) -> str:
        '''Get quality gates name'''
        pass

    # -------------------------------------------------------------------------

    def analyze(self, issues: list) -> int:
        '''Analyze each issue in list, show messages and return exit code'''

        total = len(issues)
        print('[INFO] {:4} Total'.format(total))
        if (0 == total):
            return polaris_common.EXIT_CODE_SUCCESS

        self._eval_issues(issues)

        return self._eval_rules()

    # -------------------------------------------------------------------------

    def append_rule(self, rule: AbstractRule):
        self._rules.append(rule)

    # -------------------------------------------------------------------------

    def _eval_issues(self, issues: list):
        '''Evaluate each issue in the issue list with each of the rules'''

        if (len(self._rules) == 0):
            print(f"[ERROR] rules are empty in '{self.get_name()}''")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        for issue in issues:
            for rule in self._rules:
                rule.eval(issue)

    # -------------------------------------------------------------------------

    def _eval_rules(self) -> int:
        '''Evaluate each rule, show messages and return exit code'''
        code = polaris_common.EXIT_CODE_SUCCESS

        for rule in self._rules:
            print(f"[INFO] {rule.message()}")
            if (not rule.is_OK()):
                code = polaris_common.EXIT_CODE_QUALITY_GATE_FAILED

        return code

# -----------------------------------------------------------------------------


class CustomQualityGatesAnalyzer(AbstractCustomQualityGatesAnalyzer):
    def __init__(self, name: str):
        super().__init__()
        self.__name = name

    # -------------------------------------------------------------------------

    def get_name(self) -> str:
        return self.__name
