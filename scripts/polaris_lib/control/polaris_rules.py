#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from abc import ABC, abstractmethod
import datetime

# -----------------------------------------------------------------------------

IGNORE_MAX = -1

# -----------------------------------------------------------------------------


class AbstractRule(ABC):

    @abstractmethod
    def get_name(self) -> str:
        '''Get rule name'''
        pass

    @abstractmethod
    def eval(self, issue: dict):
        '''Eval issue'''
        pass

    @abstractmethod
    def is_OK(self) -> bool:
        '''Returns True if rule is OK with all issues'''
        pass

    @abstractmethod
    def message(self) -> str:
        '''Returns OK or NOK message based in self.is_OK'''
        pass

    # -------------------------------------------------------------------------

    def _get_issue_age_in_hours(self, issue: dict, now: float) -> int:
        first_detected = issue['first_detected']

        date_time_obj = datetime.datetime.strptime(
            first_detected, '%Y-%m-%dT%H:%M:%S')

        first_detected_utc = date_time_obj.replace(
            tzinfo=datetime.timezone.utc)

        hours = (now - first_detected_utc.timestamp()) / 3600

        return hours

    # -------------------------------------------------------------------------

    def _get_issue_indicators(self, issue: dict) -> list:
        indicators = issue['indicators']
        if (indicators is None):
            return []
        return indicators.split(',')

    # -------------------------------------------------------------------------

    def _get_issue_kind(self, issue: dict) -> str:
        return issue['issue-kind']

    # -------------------------------------------------------------------------

    def _get_issue_owner(self, issue: dict) -> str:
        return issue['owner']

    # -------------------------------------------------------------------------

    def _get_issue_severity(self, issue: dict) -> str:
        return issue['severity']

    # -------------------------------------------------------------------------

    def _is_security_kind_issue(self, issue: dict) -> bool:
        issue_kind = self._get_issue_kind(issue)
        return ('security' == issue_kind) or ('various' == issue_kind)

    # -------------------------------------------------------------------------

    def _is_quality_kind_issue(self, issue: dict) -> bool:
        return 'quality' == self._get_issue_kind(issue)

    # -------------------------------------------------------------------------

    def _is_high_impact_issue(self, issue: dict) -> bool:
        return 'high' == self._get_issue_severity(issue)

    # -------------------------------------------------------------------------

    def _is_medium_impact_issue(self, issue: dict) -> bool:
        return 'medium' == self._get_issue_severity(issue)

    # -------------------------------------------------------------------------

    def _is_low_impact_issue(self, issue: dict) -> bool:
        return 'low' == self._get_issue_severity(issue)

    # -------------------------------------------------------------------------

    def _is_audit_impact_issue(self, issue: dict) -> bool:
        return 'audit' == self._get_issue_severity(issue)

    # -------------------------------------------------------------------------

    def _is_cwe_sans_issue(self, issue: dict) -> bool:
        return 'owasp-top-10' in self._get_issue_indicators(issue)

    # -------------------------------------------------------------------------

    def _is_owasp_issue(self, issue: dict) -> bool:
        return 'otop-25' in self._get_issue_indicators(issue)

    # -------------------------------------------------------------------------

    def _is_older_than(self, issue: dict, now: float, max_age_in_hours: float) -> bool:
        age = self._get_issue_age_in_hours(issue, now)
        return age > max_age_in_hours

# -----------------------------------------------------------------------------


class AbstractCountMaxRule(AbstractRule):

    def __init__(self, maxFailures: int):
        self.max_failures = maxFailures
        self.count_failures = 0

    @abstractmethod
    def get_name(self) -> str:
        '''Get rule name'''
        pass

    @abstractmethod
    def _fails(self, issue: dict) -> bool:
        '''Returns True if the issue doesn't meet the rule'''
        pass

    # -------------------------------------------------------------------------

    def eval(self, issue: dict):
        '''Eval issue'''
        if (self._fails(issue)):
            self.count_failures += 1

    # -------------------------------------------------------------------------

    def is_OK(self) -> bool:
        '''Returns True if rule is OK with all issues'''
        return (IGNORE_MAX == self.max_failures) or (self.count_failures <= self.max_failures)

    # -------------------------------------------------------------------------

    def message(self) -> str:
        if (self.is_OK()):
            return f"Rule OK : {self.get_name()}: {self.count_failures} <= {self.max_failures}"

        return f"Rule NOK: {self.get_name()}: {self.count_failures} > {self.max_failures}"


# -----------------------------------------------------------------------------


class CweSansHigh(AbstractCountMaxRule):

    def __init__(self, max: int):
        super().__init__(max)

    def get_name(self) -> str:
        return "CWE/SANS Top 25 & High"

    def _fails(self, issue: dict) -> bool:
        return (self._is_cwe_sans_issue(issue) and self._is_high_impact_issue(issue))

# -----------------------------------------------------------------------------


class OwaspHigh(AbstractCountMaxRule):

    def __init__(self, max: int):
        super().__init__(max)

    def get_name(self) -> str:
        return "OWASP Top 10 & High"

    def _fails(self, issue: dict) -> bool:
        return (self._is_owasp_issue(issue) and self._is_high_impact_issue(issue))

# -----------------------------------------------------------------------------


class QualityHigh(AbstractCountMaxRule):

    def __init__(self, max: int):
        super().__init__(max)

    def get_name(self) -> str:
        return "Quality & High"

    def _fails(self, issue: dict) -> bool:
        return (self._is_quality_kind_issue(issue) and self._is_high_impact_issue(issue))

# -----------------------------------------------------------------------------


class SecurityHigh(AbstractCountMaxRule):

    def __init__(self, max: int):
        super().__init__(max)

    def get_name(self) -> str:
        return "Security & High"

    def _fails(self, issue: dict) -> bool:
        return (self._is_security_kind_issue(issue) and self._is_high_impact_issue(issue))

# -----------------------------------------------------------------------------


class VeryOldHigh(AbstractCountMaxRule):

    def __init__(self, max: int, now: float, max_age_in_hours: float):
        super().__init__(max)
        self.now = now
        self.max_age_in_hours = max_age_in_hours

    def get_name(self) -> str:
        return "Very old & High"

    def _fails(self, issue: dict) -> bool:
        return (self._is_older_than(issue, self.now, self.max_age_in_hours))
