#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from polaris_lib import polaris_common
from polaris_lib import polaris_cli
from polaris_lib.polaris_credentials_config import PolarisCredentialsConfig
from polaris_lib.polaris_project_dir_config import PolarisProjectDirConfig

# -----------------------------------------------------------------------------


class PolarisControlConfig(PolarisCredentialsConfig, PolarisProjectDirConfig):

    def __init__(self):
        super(PolarisControlConfig, self).__init__()
        self.quality_gates = None

    # -------------------------------------------------------------------------

    def validate(self) -> int:
        code = super().validate()

        code = self.change_to_project_dir(code)

        return code

