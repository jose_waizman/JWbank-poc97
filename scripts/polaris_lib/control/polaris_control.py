#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

import sys
import time

from polaris_lib import polaris
from polaris_lib import polaris_util
from polaris_lib import polaris_common
from polaris_lib.control.polaris_control_config import PolarisControlConfig
from polaris_lib.control.polaris_quality_gates import AbstractQualityGatesAnalyzer, QualityGatesAnalyzerFactory
from polaris_lib.polaris_cli import PolarisCli
from polaris_lib.polaris_token_credential import PolarisTokenCredential

# -----------------------------------------------------------------------------

GET_ISSUES_PAGE_LIMIT = 500

# -----------------------------------------------------------------------------


class PolarisControl(PolarisCli):

    def apply(self, config: PolarisControlConfig):

        polaris_util.print_info_header('Polaris - applying quality gates in open issues')

        quality_gates_analyzer = self.__valid_params(config)

        self._login(config)
        self.__on_behalf_of()
        print(f"[INFO] Project directory '{os.getcwd()}'")
        print(f"[INFO] Quality gates '{quality_gates_analyzer.get_name()}'")
        print()

        time_start = time.time()

        polaris_token_credential = PolarisTokenCredential(config.token)

        issues = self.__getting_open_issues(config.url, polaris_token_credential.access_token, includes)
        ret_code = self.__apply_quality_gates(quality_gates_analyzer)

        polaris_util.print_elapsed_time('Export issues tooks', time_start)
        print("[INFO]")

        polaris_token_credential.dispose()

        print(f'[INFO] Exit code {ret_code}')
        sys.exit(ret_code)

    # -------------------------------------------------------------------------

    def __apply_quality_gates(self, issues_analyzer: AbstractQualityGatesAnalyzer):
        '''
        Analyze the problems of the project and branch just scanned

        Return error code
        '''

        print("[INFO] Applying quality gates ...")

        ret_code = issues_analyzer.analyze(issues)

        return ret_code

    # -------------------------------------------------------------------------

    def __getting_open_issues(self, polaris_server_url: str, access_token: str) -> list:

        print('[INFO] Getting open issues ...')

        project_id, branch_id = self._get_ids_from_local_json(polaris_server_url)

        issues = polaris.getIssues(project_id, branch_id, None, GET_ISSUES_PAGE_LIMIT, triage=false,
            polaris_server_url=polaris_server_url, access_token=access_token)

        return issues

    # -------------------------------------------------------------------------

    def __on_behalf_of(self):
        user = polaris.getJwtUser()
        if (user is None):
            print('[ERROR] User not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        organization = polaris.getJwtOrganization()
        if (organization is None):
            print('[ERROR] Organization not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        print(
            f"[INFO] Control on behalf of '{user['attributes']['name']}' from '{organization['attributes']['organizationname']}'")

    # -------------------------------------------------------------------------

    def __valid_params(self, config: PolarisControlConfig) -> AbstractQualityGatesAnalyzer:

        code = config.validate()

        if (config.quality_gates is None):
            if (code != polaris_common.EXIT_CODE_SUCCESS):
                sys.exit(code)
            return None

        factory = QualityGatesAnalyzerFactory()
        quality_gates_analyzer = factory.build(config.quality_gates)
        if (quality_gates_analyzer is None):
            ids = factory.valid_ids()
            valids = ' | '.join(factory.valid_ids())
            print(f"[Error] Unknow QUALITY_GATES '{config.quality_gates}'. Use: {valids}")
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        if (code != polaris_common.EXIT_CODE_SUCCESS):
            sys.exit(code)

        return quality_gates_analyzer

