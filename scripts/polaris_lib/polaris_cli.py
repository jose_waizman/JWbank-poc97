#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

import json
import os
import re
import requests
import subprocess
from zipfile import ZipFile

from polaris_lib import polaris
from polaris_lib import polaris_common
from polaris_lib import polaris_util
from polaris_lib.polaris_credentials_config import PolarisCredentialsConfig

# -----------------------------------------------------------------------------

POLARIS_CLI_VERSION = 'POLARIS_CLI_VERSION'
POLARIS_CLI_DIR = 'POLARIS_CLI_DIR'
POLARIS_CLI_REFRESH = 'POLARIS_CLI_REFRESH'
POLARIS_CLI_ON = 'POLARIS_CLI_ON'

POLARIS_CLI_VERSIONS = ['linux64', 'win64', 'macosx']

# -----------------------------------------------------------------------------


class PolarisCli():

    def _get_ids_from_local_json(self, polaris_server_url: str):
        '''
        Get project_id and branch_id from JSON file in current project dir

        Return project_id, branch_id
        '''

        filepath = os.path.join(os.getcwd(), '.synopsys', 'polaris', 'cli-scan.json')
        if (not os.path.exists(filepath)):
            print(f"[Error] File not found '{filepath}'")
            return polaris_common.EXIT_CODE_ERROR

        data_json = polaris_util.read_file(filepath)
        data = json.loads(data_json)
        summaryUrl = data['issueSummary']['summaryUrl']

        url = polaris_server_url
        if (url.endswith('/')):
            url += 'projects/'
        else:
            url += '/projects/'

        matches = re.search(fr"({url})(.+)(/branches/)(.+)", summaryUrl)
        groups = matches.groups()
        project_id = groups[1]
        branch_id = groups[3]

        return project_id, branch_id

    # -------------------------------------------------------------------------

    def _login(self, config: PolarisCredentialsConfig):
        if (config.token is not None):
            # convert token to JWT
            polaris.token = polaris.getJwt(config.url, config.token)
        else:
            # convert email+password to JWT
            polaris.token = polaris.getJwt(config.url, config.token, config.email, config.password)

        polaris.api = polaris.configApi(config.url)

    # -------------------------------------------------------------------------

    def _downlad_polaris_cli_tool(self, url: str, polaris_cli_version: str, polaris_cli_dir: str, refresh=True):
        '''Download the Polaris CLI tool only if it doesn't exist or refresh is True'''

        if (not refresh):
            cli_filename = 'polaris.exe' if ('win64' == polaris_cli_version) else 'polaris'
            if (os.path.exists(os.path.join(polaris_cli_dir, cli_filename))):
                return

        if (not os.path.exists(polaris_cli_dir)):
            os.mkdir(polaris_cli_dir)

        zip_filename = f'polaris_cli-{polaris_cli_version}.zip'
        url = f'{url}/api/tools/{zip_filename}'
        zip_full_filepath = os.path.join(polaris_cli_dir, zip_filename)

        print(f"[INFO] Downloading Polaris CLI Tool '{url}'")
        polaris_util.download_file(url, zip_full_filepath)

        with ZipFile(zip_full_filepath, 'r') as zip_obj:
            zip_info_list = zip_obj.infolist()
            for zip_info in zip_info_list:
                zip_info_filename = zip_info.filename
                cli_filename = os.path.basename(zip_info_filename)
                if (cli_filename != ''):
                    # Avoid extract with directory structure
                    zip_info.filename = cli_filename
                    polaris_cli_path = os.path.join(polaris_cli_dir, cli_filename)
                    print(f"[INFO] Extracting to '{polaris_cli_path}'")
                    zip_obj.extract(zip_info, polaris_cli_dir)

        print(f"[INFO] Removing '{zip_full_filepath}'")
        os.remove(zip_full_filepath)

    # -------------------------------------------------------------------------

    def _prepare_polaris_cli_env(self, polaris_server_url: str, polaris_access_token: str) -> None:
        '''
        Assign the environment variables POLARIS_SERVER_URL and POLARIS_ACCESS_TOKEN, which are
        required by polaris cli
        '''

        os.environ[polaris_common.POLARIS_SERVER_URL] = polaris_server_url
        os.environ[polaris_common.POLARIS_ACCESS_TOKEN] = polaris_access_token
