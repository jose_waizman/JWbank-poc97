#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from polaris_lib import polaris_common

# -----------------------------------------------------------------------------


class PolarisCredentialsConfig():
    def __init__(self):
        super(PolarisCredentialsConfig, self).__init__()
        self.url = None
        self.token = None
        self.email = None
        self.password = None

    # -------------------------------------------------------------------------

    def validate(self) -> int:
        code = polaris_common.EXIT_CODE_SUCCESS

        if (self.url is None):
            print(
                f'[ERROR] {polaris_common.POLARIS_SERVER_URL} must be set via environment variables or the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        if ((self.token is None) and ((self.email is None) or (self.password is None))):
            print('[ERROR] {} or ({} and {}) must be set via environment variables or the CLI'
                  .format(polaris_common.POLARIS_ACCESS_TOKEN, polaris_common.POLARIS_ACCESS_EMAIL, polaris_common.POLARIS_ACCESS_PASSWORD))
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        return code
