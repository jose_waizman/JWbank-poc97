#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from polaris_lib import polaris_common
from polaris_lib import polaris_cli
from polaris_lib.polaris_credentials_config import PolarisCredentialsConfig
from polaris_lib.polaris_project_dir_config import PolarisProjectDirConfig

# -----------------------------------------------------------------------------


class PolarisAnalyzeConfig(PolarisCredentialsConfig, PolarisProjectDirConfig):

    def __init__(self):
        super(PolarisAnalyzeConfig, self).__init__()
        self.polaris_cli_version = None
        self.polaris_cli_dir = None
        self.polaris_cli_refresh = None
        self.polaris_cli_on = None
        self.quality_gates = None

    # -------------------------------------------------------------------------

    def validate(self) -> int:
        code = super().validate()

        if (self.polaris_cli_version is None):
            print(f'[ERROR] {polaris_cli.POLARIS_CLI_VERSION} must be set via environment variables or the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        elif (not (self.polaris_cli_version in polaris_cli.POLARIS_CLI_VERSIONS)):
            valids = ' | '.join(polaris_cli.POLARIS_CLI_VERSIONS)
            print(f"[ERROR] Unknow {polaris_cli.POLARIS_CLI_VERSION} '{self.polaris_cli_version}'. Use: {valids}")
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        if (self.polaris_cli_dir is None):
            print(f'[ERROR] {polaris_cli.POLARIS_CLI_DIR} must be set via environment variables or the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        if (self.polaris_cli_refresh is None):
            print(f'[ERROR] {polaris_cli.POLARIS_CLI_REFRESH} must be set via environment variables or the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        elif (self.polaris_cli_refresh.lower() not in ['true', 'false']):
            print(f"[ERROR] Unknow {polaris_cli.POLARIS_CLI_REFRESH} '{self.polaris_cli_refresh}'. Use: true | false")
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        else:
            self.polaris_cli_refresh = self.polaris_cli_refresh.lower() == 'true'

        if (self.polaris_cli_on is None):
            print(f'[ERROR] {polaris_cli.POLARIS_CLI_ON} must be set via environment variables or the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        elif (self.polaris_cli_on.lower() not in ['true', 'false']):
            print(f"[ERROR] Unknow {polaris_cli.POLARIS_CLI_ON} '{self.polaris_cli_on}'. Use: true | false")
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        else:
            self.polaris_cli_on = self.polaris_cli_on.lower() == 'true'

        code = self.change_to_project_dir(code)

        return code

