#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

import sys
import time

from polaris_lib import polaris
from polaris_lib import polaris_util
from polaris_lib import polaris_common
from polaris_lib.analyze.polaris_analyze_config import PolarisAnalyzeConfig
from polaris_lib.polaris_cli import PolarisCli
from polaris_lib.polaris_token_credential import PolarisTokenCredential
from polaris_lib.analyze.polaris_quality_gates import AbstractQualityGatesAnalyzer, QualityGatesAnalyzerFactory

# -----------------------------------------------------------------------------


class PolarisAnalyze(PolarisCli):

    def analyze(self, config: PolarisAnalyzeConfig):

        polaris_util.print_info_header('Polaris - analyzing project with Coverity')

        issues_analyzer = self.__analyze_valid_params(config)

        self._login(config)
        self.__analyze_on_behalf_of()
        print(f"[INFO] Project directory '{os.getcwd()}'")
        if (issues_analyzer is not None):
            print(f"[INFO] Quality gates '{issues_analyzer.get_name()}'")
        print()

        time_start = time.time()

        polaris_token_credential = PolarisTokenCredential(config.token)
        config.token = polaris_token_credential.access_token

        self.__analyze_polaris_cli(time_start, config)

        ret_code = self.__analyze_issues(config.url, polaris_token_credential.access_token, issues_analyzer)

        polaris_util.print_elapsed_time('Overall duration', time_start)

        polaris_token_credential.dispose()

        if (not config.polaris_cli_on):
            print("[WARN] Polaris CLI Tool wasn't called")
            if (polaris_common.EXIT_CODE_SUCCESS == ret_code):
                print(f'[INFO] Exit code {polaris_common.EXIT_CODE_SUCCESS_BUT_IS_OFF}')
                sys.exit(polaris_common.EXIT_CODE_SUCCESS_BUT_IS_OFF)

        print(f'[INFO] Exit code {ret_code}')
        sys.exit(ret_code)

    # -------------------------------------------------------------------------

    def __analyze_apply_quality_gates(self, time_start, issues_analyzer: AbstractQualityGatesAnalyzer, issues) -> int:

        time_start = time.time()

        print("[INFO] Analyzing the issues ...")
        retCode = issues_analyzer.analyze(issues)
        polaris_util.print_elapsed_time('Analyze  issues tooks', time_start)
        print("[INFO]")

        return retCode

    # -------------------------------------------------------------------------

    def __analyze_issues(self, polaris_server_url: str, access_token: str, issues_analyzer: AbstractQualityGatesAnalyzer):
        '''
        Analyze the problems of the project and branch just scanned

        Return error code
        '''

        if (issues_analyzer is None):
            return polaris_common.EXIT_CODE_SUCCESS

        polaris_util.print_info_separator()
        print("[INFO] Applying quality gates")
        polaris_util.print_info_separator()

        time_start = time.time()
        ret_code = self.__analyze_apply_quality_gates(time_start, issues_analyzer)
        polaris_util.print_elapsed_time('Apply quality gates tooks', time_start)

        return ret_code

    # -------------------------------------------------------------------------

    def __analyze_on_behalf_of(self):
        user = polaris.getJwtUser()
        if (user is None):
            print('[ERROR] User not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        organization = polaris.getJwtOrganization()
        if (organization is None):
            print('[ERROR] Organization not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        print(
            f"[INFO] Analyze on behalf of '{user['attributes']['name']}' from '{organization['attributes']['organizationname']}'")

    # -------------------------------------------------------------------------

    def __analyze_polaris_cli(self, time_start: float, config: PolarisAnalyzeConfig) -> None:
        '''
        Performance Polaris CLI Tool analisis:
        - Prepare enviroments
        - Downlad Polaris CLI Tool if necessary
        - Call Polaris CLI Tool
        - If an access token was created for this run, delete it
        - If Polaris CLI returns an error code other than EXIT_CODE_SUCCESS, the program is exited with that code

        Requires
        - config.polaris_cli_version
        - config.polaris_cli_dir
        - config.polaris_cli_refresh
        - config.token
        - config.url
        '''

        if (not config.polaris_cli_on):
            return

        self._downlad_polaris_cli_tool(config.url, config.polaris_cli_version, config.polaris_cli_dir, config.polaris_cli_refresh)
        self._prepare_polaris_cli_env(config.url, config.token)
        ret_code = self.__call_polaris_cli(config.polaris_cli_dir)

        print()

        if (polaris_common.EXIT_CODE_SUCCESS != ret_code):
            polaris_util.print_elapsed_time('Overall duration', time_start)
            print(f'[ERROR] Exit code {ret_code}')
            sys.exit(ret_code)

        polaris_util.print_elapsed_time('Polaris analyze tooks', time_start)

    # -------------------------------------------------------------------------

    def __analyze_valid_params(self, config: PolarisAnalyzeConfig) -> AbstractQualityGatesAnalyzer:

        code = config.validate()

        if (config.quality_gates is None):
            if (code != polaris_common.EXIT_CODE_SUCCESS):
                sys.exit(code)
            return None

        issues_analyzer_factory = QualityGatesAnalyzerFactory()
        issues_analyzer = issues_analyzer_factory.build(config.quality_gates)
        if (issues_analyzer is None):
            valids = ' | '.join(issues_analyzer_factory.valid_ids())
            print(f"[Error] Unknow QUALITY_GATES '{config.quality_gates}'. Use: {valids}")
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        if (code != polaris_common.EXIT_CODE_SUCCESS):
            sys.exit(code)

        return issues_analyzer

    # -------------------------------------------------------------------------

    def __call_polaris_cli(self, polaris_cli_dir):
        polaris_cli_path = os.path.join(polaris_cli_dir, 'polaris')

        args = f'{polaris_cli_path} analyze -w --coverity-ignore-capture-failure --upload-local-config'

        return subprocess.call(args)

