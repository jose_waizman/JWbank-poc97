#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

import json
import os
import sys
import time

from polaris_lib import polaris
from polaris_lib import polaris_util
from polaris_lib import polaris_common
from polaris_lib.export.polaris_export_config import PolarisExportConfig
from polaris_lib.polaris_cli import PolarisCli
from polaris_lib.polaris_token_credential import PolarisTokenCredential

# -----------------------------------------------------------------------------

GET_ISSUES_PAGE_LIMIT = 500

# -----------------------------------------------------------------------------


class PolarisExport(PolarisCli):

    def export(self, config: PolarisExportConfig):
        '''
        Export all open issues to JSON file
        
        - Use project and branch id in the local JSON file in project directory.
        - Save them to a json file
        '''

        polaris_util.print_info_header('Polaris - exporting issues to JSON file')

        self.__valid_params(config)
        polaris_util.mkdir(config.issues_json_output)

        includes = self.__load_includes_filter()

        self._login(config)
        self.__on_behalf_of()
        print(f"[INFO] Project directory '{os.getcwd()}'")
        print()

        time_start = time.time()

        polaris_token_credential = PolarisTokenCredential(config.token)

        issues = self.__getting_open_issues(config.url, polaris_token_credential.access_token, includes)
        issues = self.__filter_fields(issues, includes)
        self.__write_issues(issues, config)

        polaris_util.print_elapsed_time('Export issues tooks', time_start)
        print("[INFO]")

        polaris_token_credential.dispose()

        ret_code = polaris_common.EXIT_CODE_SUCCESS

        print(f'[INFO] Exit code {ret_code}')
        sys.exit(ret_code)

    # -------------------------------------------------------------------------

    def __filter_fields(self, issues: list, includes: dict) -> list:

        if (includes == []):
            return issues

        print('[INFO] Filtering field issues ...')
        keys = issues[0].keys()
        excludes = [key for key in keys if key not in includes]

        if (excludes != []):
            for issue in issues:
                [issue.pop(key) for key in excludes] 

        return issues

    # -------------------------------------------------------------------------

    def __getting_open_issues(self, polaris_server_url: str, access_token: str, includes: list) -> list:

        print('[INFO] Getting open issues ...')

        project_id, branch_id = self._get_ids_from_local_json(polaris_server_url)

        triage = (('owner' in includes) or ('dismissed' in includes) or ('comment' in includes) or ('jira' in includes))

        issues = polaris.getIssues(project_id, branch_id, None, GET_ISSUES_PAGE_LIMIT, triage=triage,
            polaris_server_url=polaris_server_url, access_token=access_token)

        return issues

    # -------------------------------------------------------------------------

    def __load_includes_filter(self) -> dict:
        '''
        Return includes field list, or [] if it wasn't found
        '''
        filepath = polaris_util.build_config_filepath('export-filters.json')
        if (not os.path.exists(filepath)):
            return []

        data_json = polaris_util.read_file(filepath)
        try:
            export_filters = json.loads(data_json)
        except json.decoder.JSONDecodeError as e:
            print(f"[Error] JSON file error '{filepath}': {e.msg}")
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        return export_filters.get('includes', [])

    # -------------------------------------------------------------------------

    def __on_behalf_of(self):
        user = polaris.getJwtUser()
        if (user is None):
            print('[ERROR] User not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        organization = polaris.getJwtOrganization()
        if (organization is None):
            print('[ERROR] Organization not found')
            sys.exit(polaris_common.EXIT_CODE_ERROR)

        print(
            f"[INFO] Export on behalf of '{user['attributes']['name']}' from '{organization['attributes']['organizationname']}'")

    # -------------------------------------------------------------------------

    def __valid_params(self, config: PolarisExportConfig) -> None:

        code = config.validate()
        if (code != polaris_common.EXIT_CODE_SUCCESS):
            sys.exit(polaris_common.EXIT_CODE_PARAM_ERROR)

    # -------------------------------------------------------------------------

    def __write_issues(self, issues: list, config: PolarisExportConfig) -> None:
        print('[INFO] Writing issues to ...')
        print(f'[INFO] {config.issues_json_output}')
        polaris_util.write_file_json(config.issues_json_output, issues, config.pretty)
