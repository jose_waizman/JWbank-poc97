#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from polaris_lib import polaris_common
from polaris_lib import polaris_util

# -----------------------------------------------------------------------------


class PolarisProjectDirConfig():
    def __init__(self):
        super(PolarisProjectDirConfig, self).__init__()
        self.project_dir = None

    # -------------------------------------------------------------------------

    def change_to_project_dir(self, code) -> int:
        if (None == self. project_dir):
            print('[ERROR] PROJECT_DIR must be set via the CLI')
            code = polaris_common.EXIT_CODE_PARAM_ERROR
        elif (not polaris_util.change_dir(self.project_dir)):
            print(f"[ERROR] PROJECT_DIR doesn't exist: '{self.project_dir}'")
            code = polaris_common.EXIT_CODE_PARAM_ERROR

        return code
