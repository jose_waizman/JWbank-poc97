#!/usr/bin/python
'''
Copyright (c) 2020 Softegrity SpA (Chile). All rights reserved worldwide. The information
contained in this file is the proprietary and confidential information of
Softegrity SpA and its licensors, and is supplied subject to, and may be used
only by Synopsys customers in accordance with the terms and conditions of a
previously executed license agreement between Softegrity SpA and that customer.
'''

from polaris_lib import polaris

# -----------------------------------------------------------------------------


class PolarisTokenCredential():
    def __init__(self, access_token: str):
        '''
        Use the access_token or create a temporary access_token, which should be disposed later

        Note: If the token is None, it means that the login was with email and password, so an access
        token must be created
        '''
        if (access_token is None):
            self.access_token_id, self.access_token = polaris.createAccessToken()
        else:
            self.access_token_id = None
            self.access_token = access_token


    def dispose(self):
        '''
        Delete the access token based on its access_token_id

        If access_token_id is not None, it means that the access token was created in this session,
        so it will be removed from Polaris
        '''

        if (self.access_token_id is not None):
            if (not polaris.deleteAccessToken(self.access_token_id)):
                print(f"[WARN] Access token was not removed '{access_token_id}'")


